close all; 
clear;
clc;


% 0) 
% Od prowadz�cego 
% n = 511
% k = 220
% n - k = 291
% t = ze wzoru 33

% 1 ) Parametry wejsciowe
GF=2; % cialo binarne
m=9; % stopien cia�a rozszerzonego
n=GF^m-1; % dlugosc wektora kodowego
t=33;     % in t-ny wielomian minimalny, ile bled�w korygujemy
K=220; % in - dlugosc ci�gu kodowego ( k <= n - m*t), dlugosc informacji , k+1
d=2*t+1; % in - odleglosc minimalna (d >= 2t + 1)





% 2 ) Generator alf
% Elementy cia�a sko�czonego w postaci alf (alfa jest wielomianem)
% Dzielenia wielomianow, wiecej: Mochnacki str.33

% w postaci: 1, a1, a2 ...
alpha = gftuple([0:n-1]',m,GF);
% alpha = gftuple([0:n-1]',[1 0 0 0 1 0 0 0 0 1],GF)
% wielomian pierwotny - [1 0 0 0 1 0 0 0 0 1]






% 3) Wielomiany minimalne w postaci all - warstwy cyklotoniczne
% Spos�b z zaj�c na projekcie
i=1; g=1; k=1; p=1; ile=0;
while g<n
    while i<n
        
        %przypisanie wartosci modulo
        minPoly(g,i)=mod(p*k,n);
        i=i+1;
        k=k*2;
        
        %sprawdz czy jest cykl, jesli jest 
        %to szukaj kolejnego wielomianu minimalnego
        if minPoly(g,1) == mod(p*k,n)
            i=n;
        end
    end
    g=g+1;
    i=1;
    p=p+2;
    k=1;
   
    %pomi� powtarzaj�ce sie elementy
    if(g==(GF^m)/4+1)
       g=GF^m;
       i=GF^m;
    end
end
minPoly;
minPolyCopy=minPoly;

% usun cykl ktorej zaczynaja sie od innego elementu, 
% ale sa to takie same cykle, tylko przesuniete
u=zeros(1,GF^m-1);
[M,N] = size(minPoly);
for i=1:M
    for j=1:N
        if minPoly(i,j) > 0 && u(1,minPoly(i,j)) == 0
            u(1,minPoly(i,j))=1;
        else
            minPolyCopy(i,j)=0;
        end
    end
end
minPolyCopy;
%niepowtarzajce sie wielominay minimalne oparte na wzorach alfa
minPolyWithoutCycles = minPolyCopy(any(minPolyCopy,2),:);






% 4) Wielomiany minimalne w postaci wektorow macierzy 1 x x^2 .. 
% Mochnacki strona 36
[M,N] = size(minPolyWithoutCycles);
i=1;
mt=minPolyWithoutCycles;
k=1;

T=zeros(t,m+1);

while i<=t
    
    %warunek na stopien wielomianus
    j=1;
    koniec=1;
    while koniec==1
        if (mod((mt(i,1) * 2^j),n) == (mod(mt(i,1),n)))
            k=j;
            koniec=0;
        end
        j=j+1;
    end
 
    tempStopienWielomianu=k;
    wsp = zeros(1,k);
    %kanoniczna postac wielomianu
    for j=1:tempStopienWielomianu
        wsp(1,j)= mod((mt(i,1)*tempStopienWielomianu),n);
        tempStopienWielomianu=tempStopienWielomianu-1;
    end
   
    %stworz uklad wspolczynikow
    for j=1:k-1
        Atemp(j,:) = alpha(wsp(1,j+1)+1,:);
    end
    
    %uklad rownan w ciele gf2
    B = alpha(wsp(1,1)+1,:)';
    [MM,NN] = size(Atemp);
    Atemp(MM+1,1) = 1;
    A = Atemp';
    x = gflineq(A,B);
    x = fliplr(x');
    x(1,k+1) = 1;
    
    T(i,:) = dodaj2Wielomiany(x,T(i,:));
    
    clearvars Atemp wsp k 
    
    i=i+1;
end

%wielomiany minimalne: wolny_wyraz, x, x^2, x^3


% 5) Wielomian generujacy kod 
G=1;
for i=1:t
    G = mnoz2Wielomiany(G,T(i,:));
end
%poly2sym(fliplr(G))






% 6) Alogrytm kodowania
% strona 86 Mochnacki
G; % wielomian genrujacy

% wiadomosc
mx = zeros(1,K);
mx(1,1) = 1;
mx(1,3) = 1;
mx(1,K) = 1;
mx;


% a)
% x^(n-k), utworz wektor stopnia wielomianu generujacego
xnk(n-K+1) = 1; 
xnkMx = mnoz2Wielomiany(mx, xnk); % x^(n-k) * mx
%xnkMx = gfconv(mx,xnk) 


% b)
[qCx, rCx] = dziel2Wielomiany(xnkMx, G);
%[qCx,rCx] = gfdeconv(xnkMx,G,GF) %wynik dzielenia i reszta
%gfadd(gfconv(qCx,G),rCx)

% c)
cx = dodaj2Wielomiany(xnkMx,rCx);
wektorNadany = cx;


t=6
%przyklad blednie odkodowanego slowa przy dekoderze uproszczonym
%dekoder dedykowany potrafi skorygowac bledy
        aa1=157;
aa2=214;
aa3=46;
aa4=70;
aa5=301;


aa6=17;
aa7=292;
aa8=8;
aa9=9;
aa10=10;
aa11=11;
aa12=12;
aa13=13;
aa14=14;
aa15=15;
aa16=16;
aa17=17;
aa18=18;
aa19=19;
aa20=20;
aa21=21;
aa22=22;
aa23=23;
aa24=24;
aa25=25;
aa26=26;
aa27=27;
aa28=28;
aa29=29;
aa30=30;
aa31=31;
aa32=32;
aa33=33;

% losowy blad
cx(1,aa1)=~cx(1,aa1);
cx(1,aa2)=~cx(1,aa2);
cx(1,aa3)=~cx(1,aa3);
cx(1,aa4)=~cx(1,aa4);
cx(1,aa5)=~cx(1,aa5);
% cx(1,aa6)=~cx(1,aa6);
% cx(1,aa7)=~cx(1,aa7);
% cx(1,aa8)=~cx(1,aa8);
% cx(1,aa9)=~cx(1,aa9);
% cx(1,aa10)=~cx(1,aa10);
% cx(1,aa11)=~cx(1,aa11);
% cx(1,aa12)=~cx(1,aa12);
% cx(1,aa13)=~cx(1,aa13);
% cx(1,aa14)=~cx(1,aa14);
% cx(1,aa15)=~cx(1,aa15);
% cx(1,aa16)=~cx(1,aa16);
% cx(1,aa17)=~cx(1,aa17);
% cx(1,aa18)=~cx(1,aa18);
% cx(1,aa19)=~cx(1,aa19);
% cx(1,aa20)=~cx(1,aa20);
% cx(1,aa21)=~cx(1,aa21);
% cx(1,aa22)=~cx(1,aa22);
% cx(1,aa23)=~cx(1,aa23);
% cx(1,aa24)=~cx(1,aa24);
% cx(1,aa25)=~cx(1,aa25);
% cx(1,aa26)=~cx(1,aa26);
% cx(1,aa27)=~cx(1,aa27);
% cx(1,aa28)=~cx(1,aa28);
% cx(1,aa29)=~cx(1,aa29);
% cx(1,aa30)=~cx(1,aa30);
% cx(1,aa31)=~cx(1,aa31);
% cx(1,aa32)=~cx(1,aa32);
% cx(1,aa33)=~cx(1,aa33);
% error = cx;


% error = cx;

%oblicz syndrom nadanej wiadomosci
[qCy,syndrom] = dziel2Wielomiany(cx,G);
[sM,sN] = size(syndrom);

start = cputime;
syndrom


matrixOfSyndromsAsVector = zeros(2*t,m);
matrixOfSyndromsAsAlpha = zeros(2*t,1);
alf = zeros(n,m);
alf(1:n-1,:) = alpha(2:n,:);
alf(n,:) = alpha(1,:);
it = 1;

%Syndrom jak wektor
for i=1:2*t
    tempSyndrom = zeros(1,m);
    for s=1:length(syndrom)
        if syndrom(1,s)>0
            if s == 1
               tempSyndrom = 1; 
            else
                stopienAlf = s-1;
                if mod(i*stopienAlf,n)==0
                   tempSyndrom = dodaj2Wielomiany(tempSyndrom,alf(n,:));  
                else
                   tempSyndrom = dodaj2Wielomiany(tempSyndrom,alf(mod(i*stopienAlf,n),:));  
                end
           end
        end
    end
    matrixOfSyndromsAsVector(i,:)=tempSyndrom;
end
matrixOfSyndromsAsVector;

for i=1:2*t
    for j=1:n
        if matrixOfSyndromsAsVector(i,:) == alf(j,:)
           matrixOfSyndromsAsAlpha(i,:) = j;
        end
        
        if matrixOfSyndromsAsVector(i,:) == alf(n,:)
           matrixOfSyndromsAsAlpha(i,:) = n;
        end
        
        if matrixOfSyndromsAsVector(i,:) == 0
           matrixOfSyndromsAsAlpha(i,:) = 0;
        end
            
    end
end
matrixOfSyndromsAsAlpha


k=0;
syms double(alfa);
matrixOfSyndroms = sym(zeros(t,t+1));

for i=1:t
    for j=1:t+1
       matrixOfSyndroms(i,j)=alfa^matrixOfSyndromsAsAlpha(j+k,1);
       
       if matrixOfSyndroms(i,j) == 1
          matrixOfSyndroms(i,j) =0;
       end
    end
    k=k+1;
end
matrixOfSyndroms


koniec = 1;
tempT = t;
while koniec == 1
    
    clearvars A det;
    sumaDet=zeros(1,m);
    A = matrixOfSyndroms(1:tempT,1:tempT);
    det = det(A)
    wielomian=sym2poly(det);
    wielomian = fliplr(wielomian);
    
    for i=1:length(wielomian)
        if mod(wielomian(1,i),2) == 0
            wielomian(1,i)=0;
        end
        
        if abs(mod(wielomian(1,i),2)) == 1
            
            if i==1 || mod(i,n)==0
               sumaDet = dodaj2Wielomiany(sumaDet, alf(n,:)); 
            else
               sumaDet = dodaj2Wielomiany(sumaDet, alf(mod(i,n),:));  
            end
        end
    end
    
    if sumaDet == 0
        tempT = tempT -1;
    else
        koniec = -1;
    end
end


A = matrixOfSyndroms(1:tempT,1:tempT);
b = matrixOfSyndroms(1:tempT,tempT+1);

digitsOld = digits(32);
X = vpa(mldivide(A,b))



for i=1:tempT
    i
    itL = 0;
    itM = 0;
    
    [N,D] = numden(X(i,:));
    licznik=vpa(sym2poly(N));
    licznik=vpa(fliplr(licznik));
    sumaL = 0;
    
    for j=1:length(licznik)
        if mod( vpa(licznik(1,j)),2) == 0
            licznik(1,j)=0;
        end
        
        if abs(mod( vpa(licznik(1,j)),2)) == 1
            if j==1 || mod(j-1,n)==0
               sumaL = dodaj2Wielomiany(sumaL, alf(n,:)); 
            else
               sumaL = dodaj2Wielomiany(sumaL, alf(mod(j-1,n),:));  
            end
        end
    end
    
    mianownik= vpa(sym2poly(D));
    mianownik= vpa(fliplr(mianownik));
    sumaM = 0;
    for j=1:length(mianownik)
        if mod( vpa(mianownik(1,j)),2) == 0
            mianownik(1,j)=0;
        end
        
        if abs(mod( vpa(mianownik(1,j)),2)) == 1
            if j==1 || mod(j-1,n)==0
               sumaM = dodaj2Wielomiany(sumaM, alf(n,:)); 
            else
               sumaM = dodaj2Wielomiany(sumaM, alf(mod(j-1,n),:));  
            end
        end
    end    
    
    for j=1:n
        if sumaL == alf(j,:)
           itL = j;
        end
        
        if sumaM == alf(j,:)
           itM = j;
        end
        
        if sumaL == alf(n,:)
           itL = n;
        end
        
        if sumaM == alf(n,:)
           itM = n;
        end
        
        if sumaL == 0
           itL = 0;
        end
        
        if sumaM == 0
           itM = 0;
        end       
    end
    
    itL
    itM
        
    if itL == itM
        X(i,1) = 0;
    end
    
    if itL == 0
       X(i,1) = -1;
       itM = 0;
    end
    
    if itL > itM
    X(i,1)=itL-itM;
    end 
    
    if itM > itL
    X(i,1)=n+(itL-itM);
    end 
    
end
X

%funkcja lokalizacji chein search wersja funkcyjna
fLok = zeros(tempT+1,n);
fLok(1,:) = X(1,1);
X(tempT+1,1) = 0;

for i=2:tempT+1
    for j=1:n
        if X(i,1) > -1
            fLok(i,j) = mod(X(i,1) + (j)*(i-1), n);
        else
            fLok(i,j) = -1;
        end
    end
end

fLok

for i=1:n
    suma = 0;
    for j=1:tempT+1
        it = fLok(j,i);
        if it == -1
            continue;
        end
        
        if it == 0
            it = n;
        end
        suma = dodaj2Wielomiany(suma, alf(it,:));
    end
    
    if suma == 0
        error = mod(i+1,n);
        if error == 0 
            error = n
        else
            error
        end
    end
end
czas_dekodowania = cputime-start
koniec = 500;












% 
% 
% 
% 
% 
% 
% %%%%%%%%%%%%
% %[GGG,ttt] = bchgenpoly(15,5)
% 
% 
% 
% 
% 
% 
% 
% % A = matrixOfSyndroms(1:2,1:2)
% % b = matrixOfSyndroms(1:2,3)
% % X = (A\b)
% % 
% % [N,D] = numden(X)
% % 
% % syms x
% % eqn =   alfa^11*(alfa^4 + 1) + (alfa^13)*x + x^2;
% % c = coeffs(eqn,x)
% % e=sym2poly(c(1,1))
% % simplify(eqn,'Steps',50)
% 
% 
% 
% % Zlozone kilka bledow 2.4 do 2.5
% %http://web.ntpu.edu.tw/~yshan/BCH_code.pdf
% 
% 
% % A = sym(zeros(2, 2));
% % A(1,1) = alfa^12;
% % A(1,2) = alfa^9;
% % A(2,1) = alfa^9;
% % A(2,2) = 0;
% % 
% % %A = [alfa^12 alfa^9; alfa^9 0];
% % b = [0; alfa^3;];
% % 
% % X = (A\b)*alfa^15;
% % feval(symengine, 'degree', X(1), alfa)
% % 
% % 
% % 
