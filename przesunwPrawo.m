function wynik = przesunwPrawo(x, ileRazy)

[M,N] = size(x);
wynik = zeros(1,N);

if ileRazy > 0
    for j=1:ileRazy
        for i=2:N
            wynik(1,i)=x(1,i-1);
        end
        wynik(1,1) = x(1,N);
        x=wynik;
    end
else
    wynik = x;
end
end