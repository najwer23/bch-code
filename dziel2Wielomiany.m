function [result, reszta] = dziel2Wielomiany(dzielna, dzielnik)
  
    Ldzielna = length(dzielna);
    Ldzielnik = length(dzielnik);
    
    % ... x, x^2
    dzielna = fliplr(dzielna);
    
    %bez koncowych zer
    dzielnikTMP = dzielnik;
    iLast = find(dzielnikTMP, 1, 'last');
    dzielnik = dzielnikTMP(1,1:iLast);
    dzielnik = fliplr(dzielnik);
    LLdzielnik = length(dzielnik);
    % x^2, x ...
    
    w = zeros (1, Ldzielna);
    wynik = zeros (1, Ldzielna);
    
    j=1;
    i=1;
    w = dzielna;
    
    while i == 1
       % Sprawdz czy mozna podzielic
       if w(1,j)>0 & Ldzielna >= length(dzielnik)
           w = dodaj2Wielomiany(w,dzielnik);
           wynik(1,j) = 1;
       end
       
       %przesun dzielnik
       www = zeros (1, LLdzielnik+j);
       dzielnik = fliplr(dzielnik);
       dzielnik = dodaj2Wielomiany(www,dzielnik);
       dzielnik = fliplr(dzielnik);
       
       %warunek stopu
       if j == Ldzielna || nnz(~w(1,:)) == Ldzielna
           i = 2;
       end
       j=j+1;
    end
    
    
    %RETURN 
    % 1, x, x^2, x^3.. bez koncowych zer
    wynikTMP = fliplr(wynik);
    iLast = find(wynikTMP, 1, 'last');
    wynik = wynikTMP(1,1:iLast);
    result = wynik(1,LLdzielnik:length(wynik));
    
    resztaTMP = fliplr(w);
    if nnz(~resztaTMP(1,:)) == length(resztaTMP)
        reszta = 0;
    else
        iLast = find(resztaTMP, 1, 'last');
        reszta = resztaTMP(1,1:iLast);
    end
    
end