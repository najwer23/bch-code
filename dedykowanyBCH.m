close all; 
clear;
clc;


% 0) 
% Od prowadz�cego 
% n = 511
% k = 220
% n - k = 291
% t = ze wzoru 33






% 1 ) Parametry wejsciowe
GF=2; % cialo binarne
m=9; % stopien cia�a rozszerzonego
n=GF^m-1; % dlugosc wektora kodowego
t=33;     % in t-ny wielomian minimalny, ile bled�w korygujemy
K=220; % in - dlugosc ci�gu kodowego ( k <= n - m*t), dlugosc informacji








% 2 ) Generator alf
% Elementy cia�a sko�czonego w postaci alf (alfa jest wielomianem)
% Dzielenia wielomianow, wiecej: Mochnacki str.33
% w postaci: 1, a1, a2 ...
alpha = gftuple([0:n-1]',m,GF);
% alpha = gftuple([0:n-1]',[1 0 0 0 1 0 0 0 0 1],GF)
% wielomian pierwotny - [1 0 0 0 1 0 0 0 0 1]







% 3) W'ielomiany minimalne w postaci alf - warstwy cyklotoniczne
% Spos�b z zaj�c na projekcie
i=1; g=1; k=1; p=1; ile=0;
while g<n
    while i<n
        
        %przypisanie wartosci modulo
        minPoly(g,i)=mod(p*k,n);
        i=i+1;
        k=k*2;
        
        %sprawdz czy jest cykl, jesli jest 
        %to szukaj kolejnego wielomianu minimalnego
        if minPoly(g,1) == mod(p*k,n)
            i=n;
        end
    end
    g=g+1;
    i=1;
    p=p+2;
    k=1;
   
    %pomi� powtarzaj�ce sie elementy
    if(g==(GF^m)/4+1)
       g=GF^m;
       i=GF^m;
    end
end
minPoly;
minPolyCopy=minPoly;

% usun cykl ktorej zaczynaja sie od innego elementu, 
% ale sa to takie same cykle, tylko przesuniete
u=zeros(1,GF^m-1);
[M,N] = size(minPoly);
for i=1:M
    for j=1:N
        if minPoly(i,j) > 0 && u(1,minPoly(i,j)) == 0
            u(1,minPoly(i,j))=1;
        else
            minPolyCopy(i,j)=0;
        end
    end
end
minPolyCopy;
%niepowtarzajce sie wielominay minimalne oparte na wzorach alfa
minPolyWithoutCycles = minPolyCopy(any(minPolyCopy,2),:);






% 4) Wielomiany minimalne w postaci wektorow macierzy 1 x x^2 .. 
% Mochnacki strona 36
[M,N] = size(minPolyWithoutCycles);
i=1;
mt=minPolyWithoutCycles;
k=1;

T=zeros(t,m+1);

while i<=t
    
    %warunek na stopien wielomianu
    j=1;
    koniec=1;
    while koniec==1
        if (mod((mt(i,1) * 2^j),n) == (mod(mt(i,1),n)))
            k=j;
            koniec=0;
        end
        j=j+1;
    end
 
    tempStopienWielomianu=k;
    wsp = zeros(1,k);
    %kanoniczna postac wielomianu
    for j=1:tempStopienWielomianu
        wsp(1,j)= mod((mt(i,1)*tempStopienWielomianu),n);
        tempStopienWielomianu=tempStopienWielomianu-1;
    end
   
    %stworz uklad wspolczynikow
    for j=1:k-1
        Atemp(j,:) = alpha(wsp(1,j+1)+1,:);
    end
    
    %uklad rownan w ciele gf2
    B = alpha(wsp(1,1)+1,:)';
    [MM,NN] = size(Atemp);
    Atemp(MM+1,1) = 1;
    A = Atemp';
    x = gflineq(A,B);
    x = fliplr(x');
    x(1,k+1) = 1;
    
    T(i,:) = dodaj2Wielomiany(x,T(i,:));
    
    clearvars Atemp wsp k 
    
    i=i+1;
end
%wielomiany minimalne: wolny_wyraz, x, x^2, x^3
T;







% 5) Wielomian generujacy kod 
G=1;
for i=1:t
    G = mnoz2Wielomiany(G,T(i,:));
end
%poly2sym(fliplr(G))








% 6) Alogrytm kodowania
% strona 86 Mochnacki
G; % wielomian genrujacy

% wiadomosc
mx = zeros(1,K);
mx(1,1) = 1;
mx(1,3) = 1;
mx(1,K) = 1;
mx;


% a)
% x^(n-k), utworz wektor stopnia wielomianu generujacego
xnk(n-K+1) = 1; 
xnkMx = mnoz2Wielomiany(mx, xnk); % x^(n-k) * mx
%xnkMx = gfconv(mx,xnk) 

% b)
[qCx, rCx] = dziel2Wielomiany(xnkMx, G);
%[qCx,rCx] = gfdeconv(xnkMx,G,GF) %wynik dzielenia i reszta
%gfadd(gfconv(qCx,G),rCx)


% momentum, pomin��
%definijuj wlasny rozmiar macierzy do polieczenia
%t=3






yestest = 0;
test = 1;
start = cputime;
for ittest=1:test
    
        % c)
        cx = dodaj2Wielomiany(xnkMx,rCx);
        wektorNadany = cx;

        aa1=randi(n,1)
        aa2=randi(n,1);
        aa3=randi(n,1);
        aa4=randi(n,1);
        aa5=randi(n,1);
        aa6=randi(n,1);
        aa7=randi(n,1);
        aa8=randi(n,1);
        aa9=randi(n,1);
        aa10=randi(n,1);
        aa11=randi(n,1);
        aa12=randi(n,1);
        aa13=randi(n,1);
        aa14=randi(n,1);
        aa15=randi(n,1);
        aa16=randi(n,1);
        aa17=randi(n,1);
        aa18=randi(n,1);
        aa19=randi(n,1);
        aa20=randi(n,1);
        aa21=randi(n,1);
        aa22=randi(n,1);
        aa23=randi(n,1);
        aa24=randi(n,1);
        aa25=randi(n,1);
        aa26=randi(n,1);
        aa27=randi(n,1);
        aa28=randi(n,1);
        aa29=randi(n,1);
        aa30=randi(n,1);
        aa31=randi(n,1);
        aa32=randi(n,1);
        aa33=randi(n,1);

        % losowy blad
        cx(1,aa1)=~cx(1,aa1);
%         cx(1,aa2)=~cx(1,aa2);
%         cx(1,aa3)=~cx(1,aa3);
%         cx(1,aa4)=~cx(1,aa4);
%         cx(1,aa5)=~cx(1,aa5);
%         cx(1,aa6)=~cx(1,aa6);
%         cx(1,aa7)=~cx(1,aa7);
%         cx(1,aa8)=~cx(1,aa8);
%         cx(1,aa9)=~cx(1,aa9);
%         cx(1,aa10)=~cx(1,aa10);
%         cx(1,aa11)=~cx(1,aa11);
%         cx(1,aa12)=~cx(1,aa12);
%         cx(1,aa13)=~cx(1,aa13);
%         cx(1,aa14)=~cx(1,aa14);
%         cx(1,aa15)=~cx(1,aa15);
%         cx(1,aa16)=~cx(1,aa16);
%         cx(1,aa17)=~cx(1,aa17);
%         cx(1,aa18)=~cx(1,aa18);
%         cx(1,aa19)=~cx(1,aa19);
%         cx(1,aa20)=~cx(1,aa20);
%         cx(1,aa21)=~cx(1,aa21);
%         cx(1,aa22)=~cx(1,aa22);
%         cx(1,aa23)=~cx(1,aa23);
%         cx(1,aa24)=~cx(1,aa24);
%         cx(1,aa25)=~cx(1,aa25);
%         cx(1,aa26)=~cx(1,aa26);
%         cx(1,aa27)=~cx(1,aa27);
%         cx(1,aa28)=~cx(1,aa28);
%         cx(1,aa29)=~cx(1,aa29);
%         cx(1,aa30)=~cx(1,aa30);
%         cx(1,aa31)=~cx(1,aa31);
%         cx(1,aa32)=~cx(1,aa32);
%         cx(1,aa33)=~cx(1,aa33);
        % error = cx;

        %oblicz syndrom nadanej wiadomosci
        [qCy,syndrom] = dziel2Wielomiany(cx,G);
        [sM,sN] = size(syndrom);
       
        syndrom;

        matrixOfSyndromsAsVector = zeros(2*t,m);
        matrixOfSyndromsAsAlpha = zeros(2*t,1);
        
        %nowe ulozenie alf
        alf = zeros(n,m);
        alf(1:n-1,:) = alpha(2:n,:);
        alf(n,:) = alpha(1,:);
        it = 1;

        %Syndrom jako wektor
        %http://web.ntpu.edu.tw/~yshan/BCH_code.pdf
        for i=1:2*t
            tempSyndrom = zeros(1,m);
            for s=1:length(syndrom)
                if syndrom(1,s)>0
                    
                    %sprawdz element neutralny
                    if s == 1
                       tempSyndrom = 1; 
                    else
                        %jesli alfa
                        stopienAlf = s-1;
                        if mod(i*stopienAlf,n)==0
                           tempSyndrom = dodaj2Wielomiany(tempSyndrom,alf(n,:));  
                        else
                           tempSyndrom = dodaj2Wielomiany(tempSyndrom,alf(mod(i*stopienAlf,n),:));  
                        end
                   end
                end
            end
            matrixOfSyndromsAsVector(i,:)=tempSyndrom;
        end
        matrixOfSyndromsAsVector;

        %przedefiniuj syndromy w postaci wektor�w na alfy
        for i=1:2*t
            for j=1:n
                
                %kiedy alfa
                if matrixOfSyndromsAsVector(i,:) == alf(j,:)
                   matrixOfSyndromsAsAlpha(i,:) = j;
                end

                %kiedy 1
                if matrixOfSyndromsAsVector(i,:) == alf(n,:)
                   matrixOfSyndromsAsAlpha(i,:) = n;
                end

                %kiedy 0
                if matrixOfSyndromsAsVector(i,:) == 0
                   matrixOfSyndromsAsAlpha(i,:) = 0;
                end

            end
        end
        matrixOfSyndromsAsAlpha;

        %zbuduj macierze syndromow,
        %nowa definicja zera
        zero = -1000000;
        k=0;
        for i=1:t
            for j=1:t+1
               matrixOfSyndroms(i,j)=matrixOfSyndromsAsAlpha(j+k,1);

               if matrixOfSyndroms(i,j) == 0;
                  matrixOfSyndroms(i,j) = zero;
               end
            end
            k=k+1;
        end
        matrixOfSyndroms;



        tempT = t;
        koniec = 1;
        %wyznacznik macierzy
        while koniec == 1

            %wybierz z macierzy syndromow, kawelek t na t+1
            a = matrixOfSyndroms(1:tempT,1:tempT+1);
            [M,N] = size(a);

            %gauss elimination
            for j=1:M-1
                for i=j+1:M 
                    
                    %pivot
                    for z=i:M
                        if a(j,j) == zero 
                            
                            pivot=a(j,:);
                            a(j,:) = a(z,:);
                            a(z,:) = pivot;
                        end
                    end
                    
                    % a(i,:) = a(i,:) - a(j,:)*(a(i,j)/a(j,j)
                    %dodawnie to mnozenie,
                    %dzielenie to odejmowanie
                    wsp = a(j,:)+(a(i,j)-a(j,j));
                    
                    %gdy powstala sytuacja mnozenie 
                    %lub dzielenie przez zero
                    for k=1:N
                        if wsp(1,k) > zero && wsp(1,k) < zero + 10000
                            wsp(1,k) = zero;
                        end
                        
                        if wsp(1,k) < zero
                            wsp(1,k) = zero;
                        end
                    end

                    
                    for k=1:N
                        item1 = wsp(1,k);
                        item2 = a(i,k);

                        %przedefiniuj elementy dodawania
                        %i dodaj 2 elementy ("odejmij")
                        if item1 == zero
                            item1 = 0;
                        else
                            item1 = mod(item1,n);
                            if item1 == 0
                                item1 = n;
                            end
                            item1 = alf(item1,:);
                        end

                        if item2 == zero
                            item2 = 0;
                        else
                            item2 = mod(item2,n);
                            if item2 == 0
                                item2 = n;
                            end
                            item2 = alf(item2,:);
                        end

                        suma = dodaj2Wielomiany(item2,item1);
                        for f=1:n
                            if suma == alf(f,:)
                                suma = f;
                                break;
                            end
                            if suma == 0
                                suma = zero;
                                break;
                            end
                        end
                        suma;
                        a(i,k) = suma;

                    end
                end
            end
           
          
            % macierz trojkatna g�rna
            % sprawdz wyznacznik macierzy
            % jesli zerowy wybierz nastepna macierz (zmniejsz rozmiar)
            % jesli niezerowy przejdz dalej (wyjdz z petli)
            det=1;
            for i=1:M
                if a(i,i) == zero
                    tempItem = 0;
                else
                    item = a(i,i);
                    tempItem = alf(item,:);
                end
                det = mnoz2Wielomiany(det,tempItem);
            end
         
            if det == 0
               tempT = tempT - 1;
            else
                koniec = -1;
            end

        end

       
        %algorytm odwrotny, rozwiazanie ukladu r�wna�
        [M,N] = size(a);
        x = zeros(1,M);
        for i=M:-1:1

            suma = 0;
            for j=i+1:N
                item1 = a(i,j);
                if item1 == zero
                    item1 = 0;
                else
                    item1 = mod(item1,n);
                    if item1 == 0
                        item1 = n;
                    end
                    item1 = alf(item1,:);
                end
                suma = dodaj2Wielomiany(suma,item1);
            end

            %jesli powstalo zero zamine na wlasne zero
            for f=1:n
                if suma == alf(f,:)
                    suma = f;
                    break;
                end

                if suma == 0
                    suma = zero;
                    break;
                end
            end
            
            %jesli wynik dodawania dwoch wektorow jest rowny zero
            if suma == zero
               x(1,i) = -4242; 
            else
               x(1,i) = suma - a(i,i);
            end
            
            %aktualizuj macierz
            for j=1:i-1
                 
                %jesli x jest zerem a nie alfa^0
                if x(1,i) == -4242
                    a(j,i) = zero;
                    continue;
                end
                
                if a(j,i) == zero
                    a(j,i)=zero;
                else
                    a(j,i)=a(j,i)+x(1,i);
                end
                
            end 
        end
       
        %sprawdz czy ujemny element, zamien na dodatni
        for i=1:tempT
            if x(1,i) < 0 & x(1,i)~=-4242
                x(1,i) = n+x(1,i);
            end
        end
        
        a;
        X=x';
        %funkcja lokalizacji chein search wersja funkcyjna
        %wiki https://en.wikipedia.org/wiki/BCH_code
        fLok = zeros(tempT+1,n);
        fLok(1,:) = X(1,1);
        X(tempT+1,1) = 0;

        for i=2:tempT+1
            for j=1:n

                fLok(i,j) = mod((j)*(i-1), n);
                
                if X(i,1) > 0
                    fLok(i,j) = mod(X(i,1) + (j)*(i-1), n);  
                end
                
                if X(i,1) == 0 && i<=tempT
                    fLok(i,j) = mod((j)*(i-1),n);
                end
                
                if X(i,1) == -4242
                    fLok(i,j) = -1;
                end
            end
        end
        
        for i=1:n
            suma = 0;
            for j=1:tempT+1
                it = fLok(j,i);
                if it == -1
                    continue;
                end

                if it == 0
                    it = n;
                end
                suma = dodaj2Wielomiany(suma, alf(it,:));
            end

            if suma == 0
                error = mod(i+1,n);
                if error == 0 
                    error = n
                    cx(1,error)=~cx(1,error);
                else
                    error
                    cx(1,error)=~cx(1,error);
                end
            end
        end


        sprawdz = dodaj2Wielomiany(wektorNadany,cx);
        if nnz(~sprawdz(1,:)) == n
            ittest
            disp('Poprawnie odkodowany wektor znakow')
            yestest = 1+yestest
            %fprintf('n = %d \n',n)
        else
            disp('Blednie odkodowany wektor znakow')
        end

        czas_dekodowania = cputime-start

       
end
 
 disp("Podsumowanie----------");
 ittest
 yestest
 czas_dekodowania = cputime-start

