function wynik = mnoz2Wielomiany(x, y)
    LX = length(x);
    LY = length(y);
  
    T =  zeros(LX, LY+LX-1);

    k=0;
    for i=1:LX
        for j=1:LY
            T(i,j+k)=x(1,i)*y(1,j);
        end
        k=1+k;
    end

    wynik = T(1,:);
    for i=2:LX
        wynik = dodaj2Wielomiany(T(i,:),wynik);
    end
    
    
end