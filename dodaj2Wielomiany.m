function wynik = dodaj2Wielomiany(x, y)

LX = length(x);
LY = length(y);


if LX >= LY
    Z = LY;
    wynik = x;
else
    Z = LX;
    wynik = y;
end


for i=1:Z
    if x(i) == y(i)
        wynik(i) = 0;   
    else
        wynik(i) = 1; 
    end
end

end