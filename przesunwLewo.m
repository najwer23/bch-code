function wynik = przesunwLewo(x, ileRazy)

[M,N] = size(x);
wynik = zeros(1,N);

if ileRazy > 0
    for j=1:ileRazy
        for i=2:N
            wynik(1,i-1)=x(1,i);
        end
        wynik(1,N) = x(1,1);
        x=wynik;
    end
else
    wynik = x;
end
end