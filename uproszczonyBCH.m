close all; 
clear;
clc;

% 0) 
% Od prowadz�cego 
% n = 511
% k = 220
% n - k = 291
% t = ze wzoru 33

% 1 ) Parametry wejsciowe
GF=2; % cialo binarne
m=9; % stopien cia�a rozszerzonego
n=GF^m-1; % dlugosc wektora kodowego
t=33;% in t-ny wielomian minimalny, ile bled�w korygujemy
K=220; % in - dlugosc ci�gu kodowego ( k <= n - m*t), dlugosc informacji , k+1
d=2*t+1; % in - odleglosc minimalna (d >= 2t + 1)





% 2 ) Generator alf
% Elementy cia�a sko�czonego w postaci alf (alfa jest wielomianem)
% Dzielenia wielomianow, wiecej: Mochnacki str.33

% w postaci: 1, a1, a2 ...
alpha = gftuple([0:n-1]',m,GF);
% alpha = gftuple([0:n-1]',[1 0 0 0 1 0 0 0 0 1],GF)
% wielomian pierwotny - [1 0 0 0 1 0 0 0 0 1]






% 3) Wielomiany minimalne w postaci alf - warstwy cyklotoniczne
% Spos�b z zaj�c na projekcie
i=1; g=1; k=1; p=1; ile=0;
while g<n
    while i<n
        
        %przypisanie wartosci modulo
        minPoly(g,i)=mod(p*k,n);
        i=i+1;
        k=k*2;
        
        %sprawdz czy jest cykl, jesli jest 
        %to szukaj kolejnego wielomianu minimalnego
        if minPoly(g,1) == mod(p*k,n)
            i=n;
        end
    end
    g=g+1;
    i=1;
    p=p+2;
    k=1;
   
    %pomi� powtarzaj�ce sie elementy
    if(g==(GF^m)/4+1)
       g=GF^m;
       i=GF^m;
    end
end
minPoly;
minPolyCopy=minPoly;

% usun cykl ktorej zaczynaja sie od innego elementu, 
% ale sa to takie same cykle, tylko przesuniete
u=zeros(1,GF^m-1);
[M,N] = size(minPoly);
for i=1:M
    for j=1:N
        if minPoly(i,j) > 0 && u(1,minPoly(i,j)) == 0
            u(1,minPoly(i,j))=1;
        else
            minPolyCopy(i,j)=0;
        end
    end
end
% 17 34 68 136 272 33 66 132 264
% 33 66 132 264 17 34 68 136 272
minPolyCopy;
%niepowtarzajce sie wielominay minimalne oparte na wzorach alfa
minPolyWithoutCycles = minPolyCopy(any(minPolyCopy,2),:);






% 4) Wielomiany minimalne w postaci wektorow macierzy 1 x x^2 .. 
% Mochnacki strona 36
[M,N] = size(minPolyWithoutCycles);
i=1;
flaga=0;
mt=minPolyWithoutCycles;
while i<=t
    
    %liczba zer
    zero=nnz(~mt(i,:));
    k=N-zero;
    kk=1;
    
    %kanoniczna postac wielomianu
    for j=1:N-zero
        wsp(1,j)= mod((mt(i,1)*k),n);
        k=k-1;
    end
    wsp;
    %stworz uklad wspolczynikow
    for j=1:N-zero-1
        Atemp(j,:) = alpha(wsp(1,j+1)+1,:);
    end
    Atemp;
    %uklad rownan w ciele GF
    B = alpha(wsp(1,1)+1,:)';
    [MM,NN] = size(Atemp); 
    Atemp(MM+1,1) = 1;
    A = Atemp';
    x = gflineq(A,B);
    x = fliplr(x');
    
    x;
   
    %przeskaluj macierze dla wielomianow minimalnych
    %przeskaluj wielomiany minimalne zeby mialy ta sam� d�ugo��
    %uloz wielomiany minimalne w kolejnosci pionowej t1, t2..
    if zero > 0
        T(i,:) = [x zeros(1,zero+1)];
    else
        T(i,:) = [x zeros(1,1)];
    end
    [e, f] = size(T);
    %dodaj najstarszy wielomian
    T(i,f-zero)=1;
    
    %szukaj k do wzoru
    clearvars Atemp
    i=i+1;
end
%wielomiany minimalne: wolny_wyraz, x, x^2, x^3
T;



% 5) Wielomian generujacy kod 
G=1;
for i=1:t
    G = mnoz2Wielomiany(G,T(i,:));
end
%poly2sym(fliplr(G))






% 6) Alogrytm kodowania
% strona 86 Mochnacki
G; % wielomian genrujacy

% wiadomosc
mx = zeros(1,K);
mx(1,1) = 1;
mx(1,K) = 1;
mx(1,K-1) = 1;
mx;


% a)
% x^(n-k), utworz wektor stopnia wielomianu generujacego
xnk(n-K+1) = 1; 
xnkMx = mnoz2Wielomiany(mx, xnk); % x^(n-k) * mx
%xnkMx = gfconv(mx,xnk) 


% b)
[qCx, rCx] = dziel2Wielomiany(xnkMx, G);
%[qCx,rCx] = gfdeconv(xnkMx,G,GF) %wynik dzielenia i reszta
%gfadd(gfconv(qCx,G),rCx)


yestest = 0;
test = 1;
start = cputime;
for ittest=1:test
    
        ittest
        % c)
        cx = dodaj2Wielomiany(xnkMx,rCx);
        wektorNadany = cx;

        aa1=randi(n,1);
        aa2=randi(n,1);
        aa3=randi(n,1);
        aa4=randi(n,1);
        aa5=randi(n,1);
        aa6=randi(n,1);
        aa7=randi(n,1);
        aa8=randi(n,1);
        aa9=randi(n,1);
        aa10=randi(n,1);
        aa11=11;
        aa12=12;
        aa13=13;
        aa14=14;
        aa15=15;
        aa16=16;
        aa17=17;
        aa18=18;
        aa19=19;
        aa20=20;
        aa21=21;
        aa22=22;
        aa23=23;
        aa24=24;
        aa25=25;
        aa26=26;
        aa27=27;
        aa28=28;
        aa29=29;
        aa30=30;
        aa31=31;
        aa32=32;
        aa33=33;

        % losowy blad
        cx(1,aa1)=~cx(1,aa1);
        cx(1,aa2)=~cx(1,aa2);
%         cx(1,aa3)=~cx(1,aa3);
%         cx(1,aa4)=~cx(1,aa4);
%         cx(1,aa5)=~cx(1,aa5);
%         cx(1,aa6)=~cx(1,aa6);
%         cx(1,aa7)=~cx(1,aa7);
%         cx(1,aa8)=~cx(1,aa8);
%         cx(1,aa9)=~cx(1,aa9);
%         cx(1,aa10)=~cx(1,aa10);
        % cx(1,aa11)=~cx(1,aa11);
        % cx(1,aa12)=~cx(1,aa12);
        % cx(1,aa13)=~cx(1,aa13);
        % cx(1,aa14)=~cx(1,aa14);
        % cx(1,aa15)=~cx(1,aa15);
        % cx(1,aa16)=~cx(1,aa16);
        % cx(1,aa17)=~cx(1,aa17);
        % cx(1,aa18)=~cx(1,aa18);
        % cx(1,aa19)=~cx(1,aa19);
        % cx(1,aa20)=~cx(1,aa20);
        % cx(1,aa21)=~cx(1,aa21);
        % cx(1,aa22)=~cx(1,aa22);
        % cx(1,aa23)=~cx(1,aa23);
        % cx(1,aa24)=~cx(1,aa24);
        % cx(1,aa25)=~cx(1,aa25);
        % cx(1,aa26)=~cx(1,aa26);
        % cx(1,aa27)=~cx(1,aa27);
        % cx(1,aa28)=~cx(1,aa28);
        % cx(1,aa29)=~cx(1,aa29);
        % cx(1,aa30)=~cx(1,aa30);
        % cx(1,aa31)=~cx(1,aa31);
        % cx(1,aa32)=~cx(1,aa32);
        % cx(1,aa33)=~cx(1,aa33);


        error = cx;

        % 7) Dekodowanie
        i=0; %licznik przesuniec
        koniec = 1;
        [cxM,cxN] = size(cx);
        k = cxN;
        cd = zeros(1,cxN); %wektor skorygowany
        while koniec == 1

            %oblicz syndrom
            [qCy,syndrom] = dziel2Wielomiany(cx,G);
            [sM,sN] = size(syndrom);

            %oblicz wagi (ilosc jedynek)
            zero=nnz(~syndrom(1,:));
            jedynki = sN-zero;
            if jedynki <= t
                cd = dodaj2Wielomiany(cx,syndrom);
                cd = przesunwLewo(cd, i);
                koniec = 0;
            else
                if i == k
                disp('Dekoder uproszczony - Bledy niekorygowalne')
                koniec = 0;
                else
                cx = przesunwPrawo(cx, 1);
                i = i+1;
                end
            end


        end
        cd;
        cdOnlyMsg=cd(1,n-K+1:cxN);





        % 8) Sprawdz 
        sprawdz = dodaj2Wielomiany(wektorNadany,cd);
        if nnz(~sprawdz(1,:)) == n
            disp('Poprawnie odkodowany wektor znakow');
            %fprintf('n = %d \n',n)
            yestest = 1+yestest;
        else
            disp('Blednie odkodowany wektor znakow');
        end
       czas_dekodowania = cputime-start
end

 disp("Podsumowanie----------");
 ittest
 yestest
 czas_dekodowania = cputime-start



% A = [0 0 3;0 0 3;0 0 0]
% B = A(any(A,2),:)

% wie = [1 0 1 1 ]
% wie2 = [1 0 1]
% [wielomian,reszta] = gfdeconv(wie,wie2,2)
% c = gfconv(wie,wie2,2)

